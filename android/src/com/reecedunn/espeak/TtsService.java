/*
 * Copyright (C) 2012-2015 Reece H. Dunn
 * Copyright (C) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file implements the Android Text-to-Speech engine for eSpeak.
 *
 * Android Version: 4.0 (Ice Cream Sandwich)
 * API Version:     14
 */

package com.reecedunn.espeak;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.SynthesisCallback;
import android.speech.tts.SynthesisRequest;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeechService;
import android.util.Log;
import android.util.Pair;

import com.reecedunn.espeak.SpeechSynthesis.SynthReadyCallback;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Implements the eSpeak engine as a {@link TextToSpeechService}.
 *
 * @author msclrhd@gmail.com (Reece H. Dunn)
 * @author alanv@google.com (Alan Viverette)
 */
@SuppressLint("NewApi")
public class TtsService extends TextToSpeechService {
    public static final String ESPEAK_INITIALIZED = "com.reecedunn.espeak.ESPEAK_INITIALIZED";

    private static final String TAG = TtsService.class.getSimpleName();
    private static final boolean DEBUG = false;

    private SpeechSynthesis mEngine;
    private SynthesisCallback mCallback;

    private final Map<String, Voice> mAvailableVoices = new HashMap<String, Voice>();
    protected Voice mMatchingVoice = null;

    private BroadcastReceiver mOnLanguagesDownloaded = null;

    private AsyncExtract mAsyncExtract;
    public static final String BROADCAST_LANGUAGES_UPDATED = "com.reecedunn.espeak.LANGUAGES_UPDATED";

    @Override
    public void onCreate() {
        initializeTtsEngine();
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOnLanguagesDownloaded != null) {
            unregisterReceiver(mOnLanguagesDownloaded);
        }
        if (mAsyncExtract != null) {
            mAsyncExtract.cancel(true);
        }
    }

    /**
     * Sets up the native eSpeak engine.
     */
    private void initializeTtsEngine() {
        if (mEngine != null) {
            mEngine.stop();
            mEngine = null;
        }

        mEngine = new SpeechSynthesis(this, mSynthCallback);
        mAvailableVoices.clear();
        for (Voice voice : mEngine.getAvailableVoices()) {
            mAvailableVoices.put(voice.name, voice);
        }

        final Intent intent = new Intent(ESPEAK_INITIALIZED);
        sendBroadcast(intent);
    }

    @Override
    protected String[] onGetLanguage() {
        // This is used to specify the language requested from GetSampleText.
        if (mMatchingVoice == null) {
            return new String[]{"eng", "GBR", ""};
        }
        return new String[]{
                mMatchingVoice.locale.getISO3Language(),
                mMatchingVoice.locale.getISO3Country(),
                mMatchingVoice.locale.getVariant()
        };
    }

    private Pair<Voice, Integer> findVoice(String language, String country, String variant) {
        if (!CheckVoiceData.hasBaseResources(this) || CheckVoiceData.canUpgradeResources(this)) {
            if (mOnLanguagesDownloaded == null) {
                mOnLanguagesDownloaded = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        initializeTtsEngine();
                    }
                };

                final IntentFilter filter = new IntentFilter(DownloadVoiceData.BROADCAST_LANGUAGES_UPDATED);
                registerReceiver(mOnLanguagesDownloaded, filter);
            }

            final File dataPath = CheckVoiceData.getDataPath(this).getParentFile();

            mAsyncExtract = new AsyncExtract(this, R.raw.espeakdata, dataPath) {
                @Override
                protected void onPostExecute(Integer result) {
                    switch (result) {
                        case RESULT_OK:
                            final Intent intent = new Intent(DownloadVoiceData.BROADCAST_LANGUAGES_UPDATED);
                            sendBroadcast(intent);
                            break;
                        case RESULT_CANCELED:
                            // Do nothing?
                            break;
                    }
                }
            };

            mAsyncExtract.execute();

            return new Pair<>(null, TextToSpeech.LANG_MISSING_DATA);
        }

        final Locale query = new Locale(language, country, variant);

        Voice languageVoice = null;
        Voice countryVoice = null;

        synchronized (mAvailableVoices) {
            for (Voice voice : mAvailableVoices.values()) {
                switch (voice.match(query)) {
                    case TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:
                        return new Pair<>(voice, TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE);
                    case TextToSpeech.LANG_COUNTRY_AVAILABLE:
                        countryVoice = voice;
                    case TextToSpeech.LANG_AVAILABLE:
                        languageVoice = voice;
                        break;
                }
            }
        }

        if (languageVoice == null) {
            return new Pair<>(null, TextToSpeech.LANG_NOT_SUPPORTED);
        } else if (countryVoice == null) {
            return new Pair<>(languageVoice, TextToSpeech.LANG_AVAILABLE);
        } else {
            return new Pair<>(countryVoice, TextToSpeech.LANG_COUNTRY_AVAILABLE);
        }
    }

    private Pair<Voice, Integer> getDefaultVoiceFor(String language, String country, String variant) {
        final Pair<Voice, Integer> match = findVoice(language, country, variant);
        switch (match.second) {
            case TextToSpeech.LANG_AVAILABLE:
                if (language.equals("fr") || language.equals("fra")) {
                    return new Pair<>(findVoice(language, "FRA", "").first, match.second);
                }
                if (language.equals("pt") || language.equals("por")) {
                    return new Pair<>(findVoice(language, "PRT", "").first, match.second);
                }
                return new Pair<>(findVoice(language, "", "").first, match.second);
            case TextToSpeech.LANG_COUNTRY_AVAILABLE:
                if ((language.equals("vi") || language.equals("vie")) && (country.equals("VN") || country.equals("VNM"))) {
                    return new Pair<>(findVoice(language, country, "hue").first, match.second);
                }
                return new Pair<>(findVoice(language, country, "").first, match.second);
            default:
                return match;
        }
    }

    @Override
    protected int onIsLanguageAvailable(String language, String country, String variant) {
        return findVoice(language, country, variant).second;
    }

    @Override
    protected int onLoadLanguage(String language, String country, String variant) {
        final Pair<Voice, Integer> match = getDefaultVoiceFor(language, country, variant);
        if (match.first != null) {
            mMatchingVoice = match.first;
        }
        return match.second;
    }

    @Override
    protected Set<String> onGetFeaturesForLanguage(String lang, String country, String variant) {
        return new HashSet<String>();
    }

    @Override
    public String onGetDefaultVoiceNameFor(String language, String country, String variant) {
        final Voice match = getDefaultVoiceFor(language, country, variant).first;
        return (match == null) ? null : match.name;
    }

    @Override
    public List<android.speech.tts.Voice> onGetVoices() {
        List<android.speech.tts.Voice> voices = new ArrayList<android.speech.tts.Voice>();
        for (Voice voice : mAvailableVoices.values()) {
            int quality = android.speech.tts.Voice.QUALITY_NORMAL;
            int latency = android.speech.tts.Voice.LATENCY_VERY_LOW;
            Locale locale = new Locale(voice.locale.getISO3Language(), voice.locale.getISO3Country(), voice.locale.getVariant());
            Set<String> features = onGetFeaturesForLanguage(locale.getLanguage(), locale.getCountry(), locale.getVariant());
            voices.add(new android.speech.tts.Voice(voice.name, voice.locale, quality, latency, false, features));
        }
        return voices;
    }

    @Override
    public int onIsValidVoiceName(String name) {
        Voice voice = mAvailableVoices.get(name);
        return (voice == null) ? TextToSpeech.ERROR : TextToSpeech.SUCCESS;
    }

    @Override
    public int onLoadVoice(String name) {
        Voice voice = mAvailableVoices.get(name);
        if (voice == null) {
            return TextToSpeech.ERROR;
        }
        mMatchingVoice = voice;
        return TextToSpeech.SUCCESS;
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "Received stop request.");

        mEngine.stop();
    }

    @SuppressWarnings("deprecation")
    private String getRequestString(SynthesisRequest request) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return request.getCharSequenceText().toString();
        } else {
            return request.getText();
        }
    }

    private int selectVoice(SynthesisRequest request) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final String name = request.getVoiceName();
            if (name != null && !name.isEmpty()) {
                return onLoadVoice(name);
            }
        }

        final int result = onLoadLanguage(request.getLanguage(), request.getCountry(), request.getVariant());
        switch (result) {
            case TextToSpeech.LANG_MISSING_DATA:
            case TextToSpeech.LANG_NOT_SUPPORTED:
                return TextToSpeech.ERROR;
        }
        return TextToSpeech.SUCCESS;
    }

    @Override
    protected synchronized void onSynthesizeText(SynthesisRequest request, SynthesisCallback callback) {
        if (mMatchingVoice == null)
            return;

        String text = getRequestString(request);
        if (text == null)
            return;

        if (DEBUG) {
            Log.i(TAG, "Received synthesis request: {language=\"" + mMatchingVoice.name + "\"}");

            final Bundle params = request.getParams();
            for (String key : params.keySet()) {
                Log.v(TAG,
                        "Synthesis request contained param {" + key + ", " + params.get(key) + "}");
            }
        }

        if (text.startsWith("<?xml")) {
            // eSpeak does not recognise/skip "<?...?>" preprocessing tags,
            // so need to remove these before passing to synthesize.
            text = text.substring(text.indexOf("?>") + 2).trim();
        }

        mCallback = callback;
        mCallback.start(mEngine.getSampleRate(), mEngine.getAudioFormat(), mEngine.getChannelCount());

        final VoiceSettings settings = new VoiceSettings(PreferenceManager.getDefaultSharedPreferences(this), mEngine);
        mEngine.setVoice(mMatchingVoice, settings.getVoiceVariant());
        mEngine.Rate.setValue(settings.getRate(), request.getSpeechRate());
        mEngine.Pitch.setValue(settings.getPitch(), request.getPitch());
        mEngine.PitchRange.setValue(settings.getPitchRange());
        mEngine.Volume.setValue(settings.getVolume());
        mEngine.Punctuation.setValue(settings.getPunctuationLevel());
        mEngine.setPunctuationCharacters(settings.getPunctuationCharacters());
        mEngine.synthesize(text, text.startsWith("<speak"));
    }

    /**
     * Pipes synthesizer output from native eSpeak to an {@link AudioTrack}.
     */
    private final SpeechSynthesis.SynthReadyCallback mSynthCallback = new SynthReadyCallback() {
        @Override
        public void onSynthDataReady(byte[] audioData) {
            if ((audioData == null) || (audioData.length == 0)) {
                onSynthDataComplete();
                return;
            }

            final int maxBytesToCopy = mCallback.getMaxBufferSize();

            int offset = 0;

            while (offset < audioData.length) {
                final int bytesToWrite = Math.min(maxBytesToCopy, (audioData.length - offset));
                mCallback.audioAvailable(audioData, offset, bytesToWrite);
                offset += bytesToWrite;
            }
        }

        @Override
        public void onSynthDataComplete() {
            mCallback.done();
        }
    };


    /**
     * Begin voice data download here instead of an activity.
     */

    private static final int PROGRESS_STARTING = 0;
    private static final int PROGRESS_EXTRACTING = 1;


    private static final int RESULT_OK = -1;
    private static final int RESULT_CANCELED = -2;

    private static class ExtractProgress {
        int total;
        int progress = 0;
        int state = PROGRESS_STARTING;
        File file;

        public ExtractProgress(int total) {
            this.total = total;
        }
    }

    private static class AsyncExtract extends AsyncTask<Void, ExtractProgress, Integer> {
        private final Context mContext;
        private final int mRawResId;
        private final File mOutput;

        public AsyncExtract(Context context, int rawResId, File output) {
            mContext = context;
            mRawResId = rawResId;
            mOutput = output;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            FileUtils.rmdir(CheckVoiceData.getDataPath(mContext));

            final InputStream stream = mContext.getResources().openRawResource(mRawResId);
            final ZipInputStream zipStream = new ZipInputStream(new BufferedInputStream(stream));

            try {
                ExtractProgress progress = new ExtractProgress(stream.available());
                progress.state = PROGRESS_EXTRACTING;

                final byte[] buffer = new byte[10240];

                int bytesRead;
                ZipEntry entry;

                while (!isCancelled() && ((entry = zipStream.getNextEntry()) != null)) {
                    progress.file = new File(mOutput, entry.getName());
                    publishProgress(progress);

                    if (entry.isDirectory()) {
                        progress.file.mkdirs();
                        FileUtils.chmod(progress.file);
                        continue;
                    }

                    // Ensure the target path exists.
                    progress.file.getParentFile().mkdirs();

                    final FileOutputStream outputStream = new FileOutputStream(progress.file);
                    try {
                        while (!isCancelled() && ((bytesRead = zipStream.read(buffer)) != -1)) {
                            outputStream.write(buffer, 0, bytesRead);
                            progress.total += bytesRead;
                        }
                    } finally {
                        outputStream.close();
                    }
                    zipStream.closeEntry();

                    // Make sure the output file is readable.
                    FileUtils.chmod(progress.file);
                }

                final String version = FileUtils.read(mContext.getResources().openRawResource(R.raw.espeakdata_version));
                final File outputFile = new File(mOutput, "espeak-ng-data/version");

                FileUtils.write(outputFile, version);
                return RESULT_OK;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    zipStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return RESULT_CANCELED;
        }
    }
}
